# Info

* The tests can be executed from either of the following:

1. Terminal using "mvn test" (from project directory)
2. Running src/test/resources/testng.xml (context menu -> Run) in IDE
3. By clicking on play button next to tests/classes in IDE
