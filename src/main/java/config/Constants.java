package config;

public interface Constants {

    interface Endpoints {
        String BROWSER_URL = "http://automationpractice.com/index.php";
        String SUMMER_DRESSES_URL = BROWSER_URL + "?id_category=11&controller=category";
    }


    interface Stubs {
        String LOGIN = "login";
        String ACCOUNT_PAGE = "my-account";
        String CREATE_FORM = "create-account_form";
        String LOGIN_FORM = "login_form";
        String SUMMER_DRESSES = "product_list";
        String BLOCK_PRODUCT = "ajax_block_product";
        String PRODUCT_CONTAINER = "product-container";
        String RIGHT_BLOCK = "right-block";
        String AVAILABLE_NOW = "available-now";
        String BUTTON_CONTAINER = "button-container";
        String BUTTON = "button";
    }

}
