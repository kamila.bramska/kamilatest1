package frontend;


import config.Constants;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;

import static config.Constants.Stubs.*;

public class SunnyTests extends Base {


    @Test
    public void goToSignInPage() {

        driver.get(Constants.Endpoints.BROWSER_URL);
        Assert.assertTrue(driver.findElement(By.className(LOGIN)).isDisplayed());
        driver.findElement(By.className(LOGIN)).click();
        Assert.assertTrue(driver.getCurrentUrl().contains(ACCOUNT_PAGE));
        Assert.assertTrue(driver.findElement(By.id(CREATE_FORM)).isDisplayed());
        Assert.assertTrue(driver.findElement(By.id(LOGIN_FORM)).isDisplayed());


    }

    @Test
    public void addSummerDressToBasket() {

        driver.get(Constants.Endpoints.SUMMER_DRESSES_URL);

        Assert.assertTrue(driver.findElement(By.className(SUMMER_DRESSES)).isDisplayed());
        Assert.assertTrue(driver.findElements(By.className(SUMMER_DRESSES)).size() > 0);
        Assert.assertTrue(driver.findElements(By.className(SUMMER_DRESSES)).get(0).findElement(By.className(BLOCK_PRODUCT)).findElement(By.className(PRODUCT_CONTAINER)).findElement(By.className(RIGHT_BLOCK)).findElement(By.className(AVAILABLE_NOW)).isDisplayed());
        driver.findElements(By.className(SUMMER_DRESSES)).get(0).findElement(By.className(BLOCK_PRODUCT)).findElement(By.className(PRODUCT_CONTAINER)).findElement(By.className(RIGHT_BLOCK)).findElement(By.className(AVAILABLE_NOW)).click();
        Assert.assertTrue(driver.findElements(By.className(SUMMER_DRESSES)).get(0).findElement(By.className(BLOCK_PRODUCT)).findElement(By.className(PRODUCT_CONTAINER)).findElement(By.className(RIGHT_BLOCK)).findElement(By.className(BUTTON_CONTAINER)).findElement(By.className(BUTTON)).isDisplayed());
        driver.findElements(By.className(SUMMER_DRESSES)).get(0).findElement(By.className(BLOCK_PRODUCT)).findElement(By.className(PRODUCT_CONTAINER)).findElement(By.className(RIGHT_BLOCK)).findElement(By.className(BUTTON_CONTAINER)).findElement(By.className(BUTTON)).click();

    }


}
